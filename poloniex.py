import timee as t
import requests
import rsi as ris
import datetime

datum = []
result = None

#returns values for ether in usdt
def get_cryptocurrency_info(startdate, today_unix):
	global result
	r = requests.get('https://poloniex.com/public?command=returnChartData&currencyPair=USDT_ETH&start=' + str(startdate) + '&end='+str(today_unix) +'&period=1800')  #14400 je svaka 4h   7200 je svaka 2h
	result = r.json()
	for info in result:
		ris.close.append(info['close'])
		datum.append(datetime.datetime.fromtimestamp(info['date']).strftime('%Y-%m-%d %H:%M:%S'))


get_cryptocurrency_info(t.startdate, t.today_unix)
print(result[0])
ris.calc_rsi()

def give_results():
	first_trigger = False
	second_trigger = False
	third_trigger = False

	first_trigger_closeprice = 0
	switch1 = True
	switch2 = True

	for i in range(len(ris.rsi_points)):
		#print('rsi is ' + str(ris.rsi_points[i]) + 'on date ' + str(datum[i + 13]))
		#print('close value is ' + str(ris.close[i + 13]))

		if ris.rsi_points[i] > 70 and switch1:
			first_trigger = True
			if switch1:
				first_trigger_closeprice = ris.close[i + 13]
				switch1 = False

		if ris.rsi_points[i] < 70 and first_trigger:
			second_trigger = True
			switch2 = False

			if ris.close[i + 13] > first_trigger_closeprice:
				print('you should sell on price ' + str(ris.close[i + 13]) + 'on date ' + str(datetime.datetime.utcfromtimestamp(result[i + 13]['date'])  ))
				first_trigger = False
				second_trigger = False
				third_trigger = False
				switch1 = True
				switch2 = True
			


