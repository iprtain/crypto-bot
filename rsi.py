close = []
rsi_points = []

def calc_rsi():
	global close
	global rsi_points

	close[15] = close[14]
	rsi = 0
	change = 0
	x = 0
	y = -1
	#gain&loss total of first 14 changes
	total_gain = 0
	total_loss = 0

	changes = []

	for i in close:
		x += 1
		y += 1
		try:
			change = close[x] - close[y]
			changes.append(change)
		except:
			continue

	for i in range(14):
		if changes[i] > 0:
			total_gain += changes[i]
		else:
			total_loss = total_loss + changes[i]*-1

	first_average_gain = total_gain/14
	first_average_loss = total_loss/14

	
	gain = 0
	gains = []

	loss = 0
	losses = []

	for i in changes:
		if i > 0:
			gain += i
			gains.append(gain)
			losses.append(0)
			gain = 0
		else:
			loss += i
			losses.append(loss *-1)
			gains.append(0)
			loss = 0

	average_gains = []
	average_gains.append(first_average_gain)
	average_losses = []
	average_losses.append(first_average_loss)


	for i in range(len(gains)-13):                         ####
		average_gain = (average_gains[i]*13 + gains[i + 13])/14
		average_gains.append(average_gain)


	for i in range(len(losses)-13):
		average_loss = (average_losses[i]*13 + losses[i + 13])/14  
		average_losses.append(average_loss)

	for i in range(len(average_gains)):
		rs = average_gains[i] / average_losses[i]

		rsi = 100 - (100/(1+rs))
		rsi_points.append(rsi)


	





