import rsi as ris 
import poloniex as px 


for i in range(14):
	px.result[i]['rsi'] = 0

for i in range(len(px.result) - 14):
	px.result[i + 14]['rsi'] = ris.rsi_points[i]

for i in range(len(px.result)):
	del px.result[i]['volume']
	del px.result[i]['quoteVolume']
	del px.result[i]['weightedAverage']
	
data = []

for i in px.result:
	data.append(tuple(i.values()))


for i in data:
	print(i)